package com.iriusrisk.application

import com.iriusrisk.domain.Project
import com.iriusrisk.domain.ProjectRepository
import grails.gorm.transactions.Transactional
import org.springframework.stereotype.Component

@Component
class ProjectGetter {

    ProjectRepository projectRepository

    ProjectGetter(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository
    }

    List<Project> getAll() {
        return projectRepository.findAll()
    }

    @Transactional
    List<Project> getAllV2() {
        return Project.findAll()
    }

    Project getById(Long projectId) {
        return projectRepository.findById(projectId)
    }
}
