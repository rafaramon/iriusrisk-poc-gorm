package com.iriusrisk.application

import com.iriusrisk.domain.Project
import com.iriusrisk.domain.ProjectRepository
import org.springframework.stereotype.Component

@Component
class ProjectCreator {

    ProjectRepository projectRepository

    ProjectCreator(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository
    }

    void create(Project project) {
        projectRepository.save(project)
    }
}
