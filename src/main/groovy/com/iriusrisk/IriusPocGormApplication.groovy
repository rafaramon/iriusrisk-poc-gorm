package com.iriusrisk

import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration

@SpringBootApplication(exclude = HibernateJpaAutoConfiguration.class)
class IriusPocGormApplication implements ApplicationRunner {

    BootStrap bootStrap

    IriusPocGormApplication(BootStrap bootStrap) {
        this.bootStrap = bootStrap
    }

    static void main(String[] args) {
        SpringApplication.run(IriusPocGormApplication.class, args)
    }

    @Override
    void run(ApplicationArguments args) throws Exception {
        // Create initial data
        bootStrap.initialize()
    }
}
