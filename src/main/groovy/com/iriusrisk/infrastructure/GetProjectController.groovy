package com.iriusrisk.infrastructure

import com.iriusrisk.application.ProjectGetter
import com.iriusrisk.domain.Project
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class GetProjectController {

    ProjectGetter projectGetter

    GetProjectController(ProjectGetter projectGetter) {
        this.projectGetter = projectGetter
    }

    @GetMapping("/projects/{id}")
    ResponseEntity<Project> getProjectById(@PathVariable(value = "id") Long projectId) {
        return ResponseEntity.ok(projectGetter.getById(projectId))
    }

    @GetMapping("/projects")
    ResponseEntity<List<Project>> getProjects() {
        return ResponseEntity.ok(projectGetter.getAll())
    }

    @GetMapping("/projectsV2")
    ResponseEntity<List<Project>> getProjectsV2() {
        return ResponseEntity.ok(projectGetter.getAllV2())
    }
}
