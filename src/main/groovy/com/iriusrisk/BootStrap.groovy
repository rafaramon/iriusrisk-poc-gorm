package com.iriusrisk

import com.iriusrisk.application.ProjectCreator
import com.iriusrisk.domain.Project
import com.iriusrisk.domain.Test
import com.iriusrisk.domain.TestResult
import com.iriusrisk.domain.TestResultSource
import com.iriusrisk.domain.User
import grails.gorm.transactions.Transactional
import groovy.transform.CompileStatic
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

@Component
@CompileStatic
class BootStrap {

    static final Logger log = LoggerFactory.getLogger(BootStrap)

    ProjectCreator projectCreator

    BootStrap(ProjectCreator projectCreator) {
        this.projectCreator = projectCreator
    }

    @Transactional
    void initialize() {
        log.info("Initialize started...")

        User user1 = new User(name: "user1", desc: "User test 1")
        User user2 = new User(name: "user2", desc: "User test 2")

        Project p1 = new Project(ref: "p1", name: "project 1", desc: "description for P1", user: user1).save()
        Project p2 = new Project(ref: "p2", name: "project 2", desc: "description for P2", user: user2).save()

/*
        TestResultSource testResultSource1 = new TestResultSource(result: TestResult.FAILED)
        Test test1 = new Test(steps: "HTML_WITH_FONT_FAMILY", notes: "HTML_WITH_FONT_FAMILY",
                source: testResultSource1).save()

        Test test2 = new Test(steps: "HTML_WITH_FONT_FAMILY2", notes: "HTML_WITH_FONT_FAMILY2",
                source: testResultSource1).save()
*/

/*
        projectCreator.create(p1)
        projectCreator.create(p2)
*/
        log.info("Initialize finish!")

        log.info("Users: ${User.findAll()}")
        log.info("Projects: ${Project.findAll()}")
        log.info("TestResultSource: ${TestResultSource.findAll()}")
        log.info("Tests: ${Test.findAll()}")
    }
}
