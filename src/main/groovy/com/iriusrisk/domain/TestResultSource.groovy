package com.iriusrisk.domain

import grails.gorm.annotation.Entity
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

import java.time.LocalDateTime

@Entity
@ToString(includeNames = true, includes = ['id'])
@EqualsAndHashCode(includes = ['id'])
class TestResultSource implements Serializable {

    public static final String RESULT = "result"
    public static final String TEST = "test"

    String filename
    String args
    int type = TestType.MANUAL.ordinal()
    TestResult result = TestResult.NOTTESTED
    String output
    LocalDateTime timestamp
    boolean enabled = true
    Test test

    static constraints = {
        filename nullable: true
        args nullable: true
        output nullable: true
        timestamp nullable: true
    }

    def beforeInsert() {
        if (!timestamp) timestamp = LocalDateTime.now()
    }

    boolean hasValuesEqualTo(TestResultSource other) {
        return filename == other.filename &&
                args == other.args &&
                type == other.type &&
                timestamp == other.timestamp &&
                enabled == other.enabled
    }

    def shallowCopy(TestResultSource source = new TestResultSource()) {
        source.args = args
        source.type = type
        source.enabled = enabled
        source.filename = filename
        source.output = output
        source.result = result
        source.timestamp = timestamp
        return source
    }

    void copy(TestResultSource orig) {
        if (orig == null) return
        this.args = orig.args
        this.type = orig.type
        this.enabled = orig.enabled
        this.filename = orig.filename
        this.output = orig.output
        this.result = orig.result
        this.timestamp = orig.timestamp

    }

    public boolean isEqualsByValues(TestResultSource source) {
        return args == source.args && type == source.type && enabled == source.enabled && filename == source.filename &&
                output == source.output && result == source.result && timestamp == source.timestamp
    }

}