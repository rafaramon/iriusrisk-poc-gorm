package com.iriusrisk.domain;

public enum ProductType {
    STANDARD,
    TEMPLATE,
    LIBRARY,
    HIDDEN;
}