package com.iriusrisk.domain

import grails.gorm.annotation.Entity
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

@Entity
@ToString(includeNames = true, includes = ['id', 'name'])
@EqualsAndHashCode(includes = ['id', 'name'])
class User {

    Long id
    String name
    String desc
    Project project

    static constraints = {
        name minSize: 2, maxSize: 200
        desc nullable: true, maxSize: 2000
    }

}