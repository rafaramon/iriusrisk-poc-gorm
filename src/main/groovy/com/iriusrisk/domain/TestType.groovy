package com.iriusrisk.domain

enum TestType {
    MANUAL,
    JBEHAVE,
    JUNIT,
    SCRIPT,
    OWASPZAP,
    THREADFIX,
    CUCUMBER,
    HP_FORTIFY
}