package com.iriusrisk.domain

import grails.gorm.annotation.Entity
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

import java.time.LocalDateTime

@Entity
@ToString(includeNames = true, includes = ['id', 'steps', 'source'])
@EqualsAndHashCode(includes = ['id'])
class Test implements Serializable {

    public static final String EXPIRY_DATE = 'expiryDate'
    public static final String SOURCE = 'source'

    Long id
    String steps
    String notes
    LocalDateTime expiryDate
    int expiryPeriod = 0 //in days
    TestResultSource source //added so that it's available in unit tests
    String versionId

    Boolean udtValueUpdated = false

//    static belongsTo = [weakness: Weakness, control: Control]
    static hasOne = [source: TestResultSource]

    static mapping = {
        source lazy: false, cascade: "all-delete-orphan"
        //expiryDate type: PersistentLocalDateTime
        //control cascade: 'none'
        //weakness cascade: 'none'
    }

    static constraints = {
        steps nullable: true, maxSize: 200
        notes nullable: true, maxSize: 200
        expiryDate nullable: true
//        weakness nullable: true
//        control nullable: true
        versionId nullable: true
    }

//    static transients = ['testType', 'testResult', 'owningObject', 'udtValues', 'udtValueUpdated']

    public Test() {
        source = new TestResultSource(test: this)
    }

    void copy(Test orig) {
        if (orig == null) return

        this.steps = orig.steps
        this.notes = orig.notes
        this.expiryDate = orig.expiryDate
        this.expiryPeriod = orig.expiryPeriod
        this.source.copy(orig.getSource())
    }

    public TestResultSource getTestResultSource() {
        return source
    }

    public TestResult getTestResult() {
        if (source == null) return TestResult.NOTTESTED
        return source.result
    }

    public TestType getTestType() {
        if (source == null) return TestType.MANUAL
        return TestType.values()[source.type]
    }

/*
    Object getOwningObject() {
        if (control) return control
        return weakness
    }
*/

/*
    void updateExpiryDateFromExpiryPeriod() {
        if (expiryPeriod == 0) return
        LocalDateTime now = new LocalDateTime()
        if (expiryDate == null) {
            expiryDate = new LocalDateTime(now.year, now.monthOfYear, now.dayOfMonth, now.hourOfDay, now.minuteOfHour)
        }
        if (expiryDate.isBefore(now)) {
            expiryDate = expiryDate.plusDays(expiryPeriod)
        }
    }
*/

/*
    public static String getTestExpiryDateAsString(Test test) {
        if (test == null || test.getExpiryDate() == null) {
            return Const.NOT_APPLICABLE
        }
        DateTimeFormatter fmt = DateTimeFormat.forPattern(DateI18n.DATE_PICKER_MASK);
        return fmt.print(test.getExpiryDate());
    }
*/

    Test shallowCopy(Test test = new Test()) {
        test.notes = notes
        test.steps = steps
        test.expiryPeriod = expiryPeriod
        test.expiryDate = expiryDate

        if (source != null) {
            test.source = source.shallowCopy(test.source)
        }
        return test
    }

    public boolean equalsByValues(Test target) {
        return steps == target.steps && notes == target.notes && expiryDate == target.expiryDate &&
                expiryPeriod == target.expiryPeriod && source.isEqualsByValues(target.getSource())
    }
}
