package com.iriusrisk.domain

import groovy.transform.CompileStatic
import org.springframework.stereotype.Service

@CompileStatic
@Service
@grails.gorm.services.Service(Project)
interface ProjectRepository {

    List<Project> findAll()

    Project findById(Long projectId)

    Project save(Project project)
}