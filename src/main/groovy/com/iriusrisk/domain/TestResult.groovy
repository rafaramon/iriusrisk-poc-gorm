package com.iriusrisk.domain

enum TestResult {
    FAILED,
    ERROR,
    NOTTESTED,
    PASSED,
    NOT_APPLICABLE,
    PARTIALLY_TESTED
}