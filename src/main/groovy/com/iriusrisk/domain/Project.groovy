package com.iriusrisk.domain

import grails.gorm.annotation.Entity
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import org.grails.datastore.gorm.GormEntity

@Entity
@ToString(includeNames = true, includes = ['id', 'type', 'ref', 'name', 'revision', 'status', 'user'])
@EqualsAndHashCode(includes = ['id', 'type', 'ref', 'name', 'revision', 'status'])
class Project {

    Long id
    String ref
    String name
    String desc
    Integer priority = 0
    Long revision = 1 // Used for updating templates
    ProductType type = ProductType.STANDARD
    Status status = Status.OPEN

    static hasOne = [user: User]

    static constraints = {
        ref unique: true, minSize: 2, maxSize: 200, blank: false
        name minSize: 2, maxSize: 200
        desc nullable: true, maxSize: 2000
        priority nullable: true
        user: unique: true
    }

    public static enum Status {
        OPEN,
        CLOSED,
        DELETED,
        /**
         * @deprecated This status value has been replace by the new flag field "enabled"
         */
        DISABLED,
        VERSION
    }

}
