package com.iriusrisk

import org.grails.datastore.mapping.simple.SimpleMapDatastore
import spock.lang.Shared
import spock.lang.Specification

abstract class InMemoryDatastoreSpec extends Specification {

    @Shared
    SimpleMapDatastore simpleMapDatastore

    void setupSpec() {
        List<Class> domainClasses = getDomainClasses()
        simpleMapDatastore = new SimpleMapDatastore(domainClasses as Class[])
    }

    void cleanup() {
        simpleMapDatastore.clearData()
    }

    List<Class> getDomainClasses() { [] }
}
