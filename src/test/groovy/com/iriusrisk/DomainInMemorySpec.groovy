package com.iriusrisk

import com.iriusrisk.domain.Test
import com.iriusrisk.domain.TestResult
import com.iriusrisk.domain.TestResultSource
import grails.gorm.transactions.Rollback
import grails.gorm.transactions.Transactional
import spock.lang.Specification

class DomainInMemorySpec extends InMemoryDatastoreSpec {

    @Override
    List<Class> getDomainClasses() {
        return [TestResultSource, Test]
    }

    void "createATestWithoutErrors"() {
        given:
        TestResultSource testResultSource1 = new TestResultSource(result: TestResult.FAILED)
        Test test1 = new Test(steps: "HTML_WITH_FONT_FAMILY", notes: "HTML_WITH_FONT_FAMILY",
                source: testResultSource1).save(flush: true)

        Test test2 = new Test(steps: "HTML_WITH_FONT_FAMILY2", notes: "HTML_WITH_FONT_FAMILY2",
                source: testResultSource1).save(flush: true)

        when:
        int countTestResultResources = TestResultSource.findAll().size()
        int countTests = Test.findAll().size()

        then:
        countTestResultResources == 1
        countTests == 2
    }
}
