package com.iriusrisk

import com.iriusrisk.domain.Test
import com.iriusrisk.domain.TestResult
import com.iriusrisk.domain.TestResultSource
import grails.gorm.transactions.Rollback
import org.assertj.core.api.Assertions
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
@Rollback
class DomainIntegrationTest {

    @org.junit.jupiter.api.Test
    void createATestWithoutErrors() {
        TestResultSource testResultSource1 = new TestResultSource(result: TestResult.FAILED)
        Test test1 = new Test(steps: "HTML_WITH_FONT_FAMILY", notes: "HTML_WITH_FONT_FAMILY",
                source: testResultSource1).save()

        Test test2 = new Test(steps: "HTML_WITH_FONT_FAMILY2", notes: "HTML_WITH_FONT_FAMILY2",
                source: testResultSource1).save()

        int countTestResultResources = TestResultSource.findAll().size()
        int countTests = Test.findAll().size()

        Assertions.assertThat(countTestResultResources).isEqualTo(1)
        Assertions.assertThat(countTests).isEqualTo(2)
    }
}
