package com.iriusrisk

import com.iriusrisk.domain.Test
import com.iriusrisk.domain.TestResult
import com.iriusrisk.domain.TestResultSource
import spock.lang.Specification

class DomainUnitSpec extends Specification {

    void setup() {
        GroovyMock(Test, global: true)
        GroovyMock(TestResultSource, global: true)
    }

    void "createATestWithoutErrors"() {
        given:
        TestResultSource testResultSource1 = new TestResultSource(result: TestResult.FAILED)
        Test test1 = new Test(steps: "HTML_WITH_FONT_FAMILY", notes: "HTML_WITH_FONT_FAMILY",
                source: testResultSource1)

        Test test2 = new Test(steps: "HTML_WITH_FONT_FAMILY2", notes: "HTML_WITH_FONT_FAMILY2",
                source: testResultSource1)

        Test.findAll() >> [test1, test2]
        TestResultSource.findAll() >> [testResultSource1]

        when:
        int countTestResultResources = TestResultSource.findAll().size()
        int countTests = Test.findAll().size()

        then:
        countTestResultResources == 1
        countTests == 2
    }
}
